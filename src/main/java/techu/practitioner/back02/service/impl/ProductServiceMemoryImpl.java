package techu.practitioner.back02.service.impl;

import org.springframework.stereotype.Service;
import techu.practitioner.back02.model.Product;
import techu.practitioner.back02.service.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class ProductServiceMemoryImpl implements ProductService {

    ConcurrentMap<String, Product> products = new ConcurrentHashMap<>();

    @Override
    public String addProduct(Product p) {
        String id = UUID.randomUUID().toString();
        p.id = id;
        products.put(id, p);
        return id;
    }

    @Override
    public List<Product> getAllProducts() {
        return new ArrayList<>(products.values());
    }

    @Override
    public List<Product> getAllProductsWithoutUsers() {
        return products.values().stream().filter(p -> p.users.isEmpty()).collect(Collectors.toList());
    }

    @Override
    public Product getProduct(String id) {
        return products.get(id);
    }

    @Override
    public void updateProduct(String id, Product p) {
        p.id = id;
        products.put(id, p);
    }
}
