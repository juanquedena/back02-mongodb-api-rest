package techu.practitioner.back02.controller;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.back02.auth.AuthHandler;
import techu.practitioner.back02.model.User;
import techu.practitioner.back02.service.UserService;

import java.util.Arrays;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Before(@BeforeElement(AuthHandler.class))
@RestController
@RequestMapping(ApiRoutes.USER)
@CrossOrigin
public class UserController {
    
    @Autowired
    UserService userService;

    @GetMapping
    public EntityModel<User> getUser(@PathVariable(name = "userId") String id) {
        return createResponseUser(findUserById(id));
    }

    @PutMapping
    public void replaceUser(@PathVariable(name = "userId") String id,
                            @RequestBody UserInput p) {
        final User x = findUserById(id); // Si no existe, lanza excepcion 404.

        if(p.role == null || p.role.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        x.role = p.role;
        this.userService.updateUser(id, x);
    }

    @PatchMapping
    public void updateUser(@PathVariable(name = "userId") String id,
                           @RequestBody UserInput p) {
        final User x = findUserById(id);
        if(p.role != null) {
            if(p.role.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.role = p.role;
        }
        this.userService.updateUser(id, x);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable(name = "userId") String id) {
        this.userService.deleteUser(id);
    }

    private User findUserById(String UserId) {
        final User p = this.userService.getUser(UserId);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    private EntityModel<User> createResponseUser(User p) {
        return EntityModel.of(p).add(createLinkUser(p));
    }

    private List<Link> createLinkUser(User p) {
        final Link Users = linkTo(methodOn(UsersController.class).getAllUsers())
                .withRel("users")
                .withTitle("Lista de todos los usuarios");

        return Arrays.asList(
                getUrlUser(p),
                Users
        );
    }

    private Link getUrlUser(User p) {
        return linkTo(methodOn(UserController.class).getUser(p.userId))
                .withSelfRel()
                .withTitle("Ver detalles de este usuario");
    }

    public static class UserInput {

        public String role;
    }
}
