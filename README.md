# BACK02 - Integración operaciones CRUD en MongoDB - API REST #

### Casos de uso ###

* Los consumidores podrán obtener el listado de productos disponibles.
* Se podrá obtener información detallada de cada producto: id, descripción y precio.
* Se almacenará información de los usuarios de productos: userId y rol.
* Cada producto posee información acerca de los usuarios que lo usan.
* Se podrán crear, actualizar y borrar usuarios.
* Incluiremos las operaciones básicas consultar, añadir y actualizar productos.
* No se podrán eliminar productos.
* Se podrán filtrar productos que no tengan clientes.

### Codelabs Auxiliares Usados ###

* **APIEX01** - Diseñar y modelar una API REST con RAML **[Ver especificación](spec.md)**
* **APIEX11** - Seguridad: Autenticación API REST basada en tokens con Spring y JWT
* **APIEX12** - Servicio REST con Spring y anotación @CrossOrigin
* **APIEX13** - Documentación de nuestra API (Swagger + Swagger UI Tool)

### Codelabs Retos Usados ###

* **CL02** - JWT avanzado
* **CL03** - Docker

> ### Configuración de Docker ###
> #### Metodo 1 #### 
> 1. Dockerfile
> ```
> FROM openjdk:16-jdk-alpine
> RUN addgroup -S spring && adduser -S spring -G spring
> USER spring:spring
> ARG JAR_FILE=target/*.jar
> COPY ${JAR_FILE} app.jar
> ENTRYPOINT ["java","-jar","/app.jar"]
> ``` 
> 2. Compilación del proyecto
> ```
> ./mvnw package
> ```
> 3. Compilación de la imagen
> ```
> docker build -t techu/back02 .
> ```
> 4. Creación del contenero
> ```
> docker run -ti -p 8080:8081 -d techu/back02
> ```
> #### Metodo 2 ####
> 1. Usuando el plugin de spring-boot
> ```
> ./mvnw spring-boot:build-image -Dspring-boot.build-image.imageName=techu/back02
> ```

### Herramientas complementarias ###

#### raml2html ####

Un simple generador de documentación de RAML a HTML, escrito para Node.js.

Instalación
```
npm install -g raml2html raml2html-markdown-theme
```

Generar documentación en formato html
```
raml2html spec.raml > spec.html
```

Generar documentación en formato markdown
```
raml2html --theme raml2html-markdown-theme spec.raml > spec.md
```

### Pruebas con Postman ###

* [Entorno](https://bitbucket.org/juanquedena/back02-mongodb-api-rest/src/master/src/test/resources/TechU-Backend-Local.postman_environment.json)
* [Colleción de casos](https://bitbucket.org/juanquedena/back02-mongodb-api-rest/src/master/src/test/resources/TechU-Practitioner-Back01.postman_collection.json)
