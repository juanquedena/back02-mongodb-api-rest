package techu.practitioner.back02.controller;

public class ApiRoutes {

    public static final String API_VERSION = "/stocks/v1";
    public static final String AUTHENTICATE     = API_VERSION + "/authenticate";
    public static final String PRODUCTS         = API_VERSION + "/products";
    public static final String PRODUCT          = API_VERSION + "/products/{productId}";
    public static final String PRODUCT_USERS    = API_VERSION + "/products/{productId}/users";
    public static final String PRODUCT_USER     = API_VERSION + "/products/{productId}/users/{userId}";
    public static final String USERS            = API_VERSION + "/users";
    public static final String USER             = API_VERSION + "/users/{userId}";
}
