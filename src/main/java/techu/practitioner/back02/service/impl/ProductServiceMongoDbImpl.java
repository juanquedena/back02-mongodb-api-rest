package techu.practitioner.back02.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import techu.practitioner.back02.model.Product;
import techu.practitioner.back02.service.ProductService;
import techu.practitioner.back02.service.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceMongoDbImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public String addProduct(Product p) {
        productRepository.insert(p);
        return p.id;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> getAllProductsWithoutUsers() {
        return null; // products.values().stream().filter(p -> p.users.isEmpty()).collect(Collectors.toList());
    }

    @Override
    public Product getProduct(String id) {
        final Optional<Product> p = this.productRepository.findById(id);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public void updateProduct(String id, Product p) {
        p.id = id;
        this.productRepository.save(p);
    }
}
