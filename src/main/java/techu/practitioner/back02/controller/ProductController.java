package techu.practitioner.back02.controller;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.back02.auth.AuthHandler;
import techu.practitioner.back02.model.Product;
import techu.practitioner.back02.service.ProductService;

import java.util.Arrays;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Before(@BeforeElement(AuthHandler.class))
@RestController
@RequestMapping(ApiRoutes.PRODUCT)
@CrossOrigin
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping
    public EntityModel<Product> getProduct(@PathVariable(name = "productId") String id) {
        return createResponseProduct(findProductById(id));
    }

    @PutMapping
    public void replaceProduct(@PathVariable(name = "productId") String id,
                               @RequestBody InputProduct p) {
        final Product x = findProductById(id); // Si no existe, lanza excepcion 404.

        if(p.description == null || p.description.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(p.price == null || p.price < 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        x.description = p.description;
        x.price = p.price;

        this.productService.updateProduct(id, x);
    }

    @PatchMapping
    public void updateProduct(@PathVariable(name = "productId") String id,
                              @RequestBody InputProduct p) {
        final Product x = findProductById(id);
        if(p.description != null) {
            if(p.description.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.description = p.description;
        }
        if(p.price != null) {
            if(p.price < 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.price = p.price;
        }

        this.productService.updateProduct(id, x);
    }

    private Product findProductById(String productId) {
        final Product p = this.productService.getProduct(productId);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    private EntityModel<Product> createResponseProduct(Product p) {
        return EntityModel.of(p).add(createLinkProduct(p));
    }

    private List<Link> createLinkProduct(Product p) {
        final Link products = linkTo(methodOn(ProductsController.class).getAllProducts())
                .withRel("products")
                .withTitle("Lista de todos los productos");

        final Link users = linkTo(methodOn(ProductUsersController.class).getUsersForProduct(p.id))
                .withRel("users")
                .withTitle("Lista de usuarios");

        return Arrays.asList(
            getUrlProduct(p),
            products,
            users
        );
    }

    private Link getUrlProduct(Product p) {
        return linkTo(methodOn(ProductController.class).getProduct(p.id))
                .withSelfRel()
                .withTitle("Ver detalles de este producto");
    }

    public static class InputProduct {

        public String description;
        public Double price;
    }
}
