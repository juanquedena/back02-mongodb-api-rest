package techu.practitioner.back02.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.back02.auth.JwtBuilder;

@RestController
@RequestMapping(ApiRoutes.AUTHENTICATE)
public class AuthController {

    @Autowired
    JwtBuilder jwtBuilder;

    @PostMapping
    public String generateToken(@RequestBody Auth auth){
        if(!"techU".equals(auth.userId)
                || !"p4ssw0rd+#".equals(auth.passwd)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        return jwtBuilder.generateToken(auth.userId,"admin");
    }

    public static class Auth {
        public String userId;
        public String passwd;
    }
}
