package techu.practitioner.back02.controller;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import techu.practitioner.back02.auth.AuthHandler;
import techu.practitioner.back02.model.Product;
import techu.practitioner.back02.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Before(@BeforeElement(AuthHandler.class))
@RestController
@RequestMapping(ApiRoutes.PRODUCTS)
@CrossOrigin
public class ProductsController {

    @Autowired
    ProductService productService;

    @GetMapping
    public CollectionModel<EntityModel<Product>> getAllProducts() {
        List<Product> products = this.productService.getAllProducts();
        return CollectionModel.of(products.stream().map(x -> createResponseProduct(x)).collect(Collectors.toList()))
                .add(linkTo(methodOn(this.getClass()).getAllProducts()).withSelfRel());
    }

    @PostMapping
    public ResponseEntity addProduct(@RequestBody Product p) {
        this.productService.addProduct(p);
        return ResponseEntity
                .ok()
                .location(getUrlProduct(p).toUri())
                .build();
    }

    private EntityModel<Product> createResponseProduct(Product p) {
        return EntityModel.of(p).add(getUrlProduct(p));
    }

    private Link getUrlProduct(Product p) {
        return linkTo(methodOn(ProductController.class).getProduct(p.id))
                .withSelfRel()
                .withTitle("Ver detalles de este producto");
    }
}
