package techu.practitioner.back02.service;

import techu.practitioner.back02.model.User;

import java.util.List;

public interface UserService {

    String addUser(User u);

    List<User> getAllUsers();

    User getUser(String userId);

    void updateUser(String userId, User u);

    void deleteUser(String userId);
}
