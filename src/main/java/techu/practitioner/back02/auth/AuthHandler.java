package techu.practitioner.back02.auth;

import com.kastkode.springsandwich.filter.api.BeforeHandler;
import com.kastkode.springsandwich.filter.api.Flow;
import org.jose4j.jwt.JwtClaims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthHandler implements BeforeHandler {
    Logger logger = LoggerFactory.getLogger(AuthHandler.class);

    @Autowired
    JwtBuilder jwtBuilder;

    @Override
    public Flow handle(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler, String[] flags)
            throws Exception{
        logger.debug(request.getMethod() + " request is executing on " + request.getRequestURI());
        String token = request.getHeader("Authorization");

        if (token == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Auth token is required");
        }

        try {
            JwtClaims claims = jwtBuilder.generateParseToken(token.substring(7).trim());
            logger.debug("Authorization: userID = " + claims.getClaimValue("userID"));
            logger.debug("Authorization: expirationTime = " + claims.getExpirationTime());
            request.setAttribute("userId",claims.getClaimValue("userID").toString());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage(), e);
        }

        // check if the user is still valid
        return Flow.CONTINUE;
    }
}