package techu.practitioner.back02.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.back02.auth.AuthHandler;
import techu.practitioner.back02.model.Product;
import techu.practitioner.back02.model.User;
import techu.practitioner.back02.service.ProductService;
import techu.practitioner.back02.service.UserService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Before(@BeforeElement(AuthHandler.class))
@RestController
@RequestMapping(ApiRoutes.PRODUCT_USERS)
@CrossOrigin
public class ProductUsersController {
    
    @Autowired
    ProductService productService;

    @Autowired
    UserService userService;

    @GetMapping
    public CollectionModel<EntityModel<ProductUser>> getUsersForProduct(@PathVariable(name = "productId") String id) {
        final Product p = this.findProductById(id);
        final List<String> usersId = p.users;

        return CollectionModel.of(usersId.stream().map(
                userId -> EntityModel
                        .of(new ProductUser())
                        .add(linkTo(methodOn(UserController.class).getUser(userId))
                                .withRel("user")
                                .withTitle("Usuario de este producto"))
        ).collect(Collectors.toUnmodifiableList())).add(Arrays.asList(
                linkTo(methodOn(this.getClass()).getUsersForProduct(id)).withSelfRel()
        ));
    }

    @PutMapping
    public void addReferenceUser(@PathVariable(name = "productId") String id,
                                 @RequestBody UserInput p) {
        final Product x = this.findProductById(id);
        final User v = this.userService.getUser(p.userId);
        if(v == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        for(String z : x.users) {
            if(z.equalsIgnoreCase(p.userId))
                return;
        }

        x.users.add(p.userId);
        this.productService.updateProduct(id, x);
    }

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteReferenceUser(@PathVariable(name = "productId") String id,
                                    @PathVariable String userId) {
        final Product x = this.findProductById(id);
        boolean encontrado = false;
        int j = 0;
        for(int i = 0; i < x.users.size(); i++) {
            if(x.users.get(i).equalsIgnoreCase(userId)) {
                j = i;
                encontrado = true;
            }
        }
        if(encontrado) {
            x.users.remove(j);
            this.productService.updateProduct(id, x);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private Product findProductById(String productId) {
        final Product p = this.productService.getProduct(productId);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    public static class UserInput {

        public String userId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ProductUser {

        public String role;
    }
}
