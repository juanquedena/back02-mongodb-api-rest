package techu.practitioner.back02.service.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import techu.practitioner.back02.model.Product;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {
}
