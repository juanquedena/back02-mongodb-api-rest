package techu.practitioner.back02.controller;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import techu.practitioner.back02.auth.AuthHandler;
import techu.practitioner.back02.model.User;
import techu.practitioner.back02.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Before(@BeforeElement(AuthHandler.class))
@RestController
@RequestMapping(ApiRoutes.USERS)
@CrossOrigin
public class UsersController {

    @Autowired
    UserService userService;

    @GetMapping
    public CollectionModel<EntityModel<User>> getAllUsers() {
        List<User> users = this.userService.getAllUsers();
        return CollectionModel.of(users.stream().map(x -> createResponseUser(x)).collect(Collectors.toList()))
                .add(linkTo(methodOn(this.getClass()).getAllUsers()).withSelfRel());
    }

    @PostMapping
    public ResponseEntity addUser(@RequestBody User p) {
        this.userService.addUser(p);
        return ResponseEntity
                .ok()
                .location(getUrlUser(p).toUri())
                .build();
    }

    private EntityModel<User> createResponseUser(User p) {
        return EntityModel.of(p).add(getUrlUser(p));
    }

    private Link getUrlUser(User p) {
        return linkTo(methodOn(UserController.class).getUser(p.userId))
                .withSelfRel()
                .withTitle("Ver detalles de este Usuario");
    }
}
