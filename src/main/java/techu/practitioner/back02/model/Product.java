package techu.practitioner.back02.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection="back02_products")
public class Product {

    @JsonIgnore
    public String id;
    public String description;
    public double price;

    @JsonIgnore
    public List<String> users = new ArrayList<>();
}
