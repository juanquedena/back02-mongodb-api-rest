package techu.practitioner.back02.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import techu.practitioner.back02.model.User;
import techu.practitioner.back02.service.UserService;
import techu.practitioner.back02.service.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceMongoDbImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public String addUser(User u) {
        userRepository.insert(u);
        return u.userId;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(String userId) {
        final Optional<User> p = userRepository.findById(userId);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public void updateUser(String userId, User u) {
        u.userId = userId;
        userRepository.save(u);
    }

    @Override
    public void deleteUser(String userId) {
        userRepository.deleteById(userId);
    }
}
